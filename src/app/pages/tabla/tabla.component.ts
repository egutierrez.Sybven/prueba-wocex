import { Component, OnInit } from '@angular/core';
import { TareaService } from '../../services/tarea.service';
import { AgregarModel } from '../../models/agregar.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tabla.component',
  templateUrl: './tabla.component.component.html',
  styleUrls: ['./tabla.component.component.css']
})
export class TablaComponent implements OnInit {

  tareas:AgregarModel[]=[];
  cargando=false;

  constructor(private tareaService:TareaService) { }

  ngOnInit() {
    this.cargando=true;
    this.tareaService.getTareas().subscribe(resp=> {
      this.tareas = resp;
      this.cargando=false;
    });
    
  }
  deleteTarea(tareas:AgregarModel, i:number){
    Swal.fire({
      title:'¿Está seguro?',
      text:`Esta seguro que desea borrar el registro: ${tareas.nombre}`,
      type:'question',
      showConfirmButton:true,
      showCancelButton:true
    }).then(resp=>{

      if(resp.value){
        this.tareas.splice(i ,1 );
        this.tareaService.deleteTarea(tareas.id).subscribe();
      }
    })
  }  

} 
