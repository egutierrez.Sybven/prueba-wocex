import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AgregarModel } from '../../models/agregar.model';
import { TareaService } from '../../services/tarea.service';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  tarea:AgregarModel=new AgregarModel();
  id: string;

  constructor(
    private tareaService:TareaService,
    private route:ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id !== 'nuevo') {
      this.tareaService.getTarea(this.id).
      subscribe((resp:AgregarModel) => {
        this.tarea = resp;
        this.tarea.id = this.id;
      });
    }
  }

  guardar(form:NgForm){
   if (form.invalid) {
      console.log('formulario invalido');
      return;
   }

  Swal.fire({
    title: 'Espere',
    text: 'Guardando Informacion',
    type: 'info',
    allowOutsideClick: false
    });
    Swal.showLoading();

    let peticion:Observable<any>;

  if (this.tarea.id) {

    peticion = this.tareaService.actualizarTarea(this.tarea);

  } else {

    peticion = this.tareaService.crearTarea(this.tarea);
  }

  peticion.subscribe(() => {
      Swal.fire({
        title:this.tarea.nombre,
        text: `Se ${this.id !== 'nuevo'? 'actualizó': 'creó'} correctamente`,
        type: 'success'
      })
    })
    
    this.router.navigate(['table'])
  }
}
