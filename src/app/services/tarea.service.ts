import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AgregarModel } from '../models/agregar.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TareaService {

  private url = 'https://login-app-e2b2d-default-rtdb.firebaseio.com'
  constructor(private http: HttpClient) { }

  crearTarea(tarea:AgregarModel) {
    return this.http.post(`${this.url}/tarea.json`,tarea).pipe(map ((resp: any) => {
      tarea.id= resp.name;
      return tarea;
    })
    )
  }
  
  actualizarTarea(tarea:AgregarModel) {
    const tareaTemp = {
     ...tarea
    };
    delete tareaTemp.id;
    return this.http.put(`${this.url}/tarea/${tarea.id}.json`,tareaTemp);
  }

  getTarea(id:string) {
    return this.http.get(`${this.url}/tarea/${id}.json`);
  }

  deleteTarea(id:string) {
    return this.http.delete(`${this.url}/tarea/${id}.json`);
  }

  getTareas() { 
    return this.http.get(`${this.url}/tarea.json`).pipe(
      map(this.crearArreglo)
    );
  }

  private crearArreglo( tareaObj: object ) {
    const tareas: AgregarModel[] = [];
    
    if(tareaObj !== null) {
      Object.keys( tareaObj ).forEach( key => {
    
        const tarea: AgregarModel = tareaObj[key];
        tarea.id = key;
    
        tareas.push( tarea );
      });
    }
  
    return tareas;
  }
  
}
