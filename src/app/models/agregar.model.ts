export class AgregarModel {

    id: string;
    nombre: string;
    descripcion: string;
    estado: boolean;

    constructor() {
        this.estado = false;
    }
}
